/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
function getInformation(){
	let fullName = prompt("Enter your fullname: ");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter your location: ");

    console.log('Hi ' +fullName);
    console.log(fullName +'\'s age is ' +age);
    console.log(fullName +' is located at ' +location);
};
getInformation();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function printBand(){
    let favoriteBand = ['Imagine Dragons', 'Panic! At The Disco', 'The Vamps', 'My Chemical Romance','5SOS']
    console.log(favoriteBand);
}
printBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function printMovies(){
    let movies = ['Interstellar','A Silent Voice','Up','Kubo and the Two Strings','Everything Everywhere All at Once'];
    let ratings = ['73%', '95%','98%','97%','95%'];

    console.log('1. ' +movies[0]);
    console.log('Tomatometer for ' +movies[0] +': ' +ratings[0]);

    
    console.log('2. ' +movies[1]);
    console.log('Tomatometer for ' +movies[1] +': ' +ratings[1]);

    
    console.log('3. ' +movies[2]);
    console.log('Tomatometer for ' +movies[2] +': ' +ratings[2]);

    
    console.log('4. ' +movies[3]);
    console.log('Tomatometer for ' +movies[3] +': ' +ratings[3]);

    
    console.log('5. ' +movies[4]);
    console.log('Tomatometer for ' +movies[4] +': ' +ratings[4]);
}
printMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();